# dotfiles (for shell)

Uses GNU Stow for management (except `motd`). I try to cite my sources to the best of my ability,
but sometimes I can't figure out where I find these things.

## External Resources
* `nano`
  * [`.nanorc`](https://github.com/scopatz/nanorc)

## Sources
* `neofetch`
  * `monika_chibi.png`
