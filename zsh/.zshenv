#
# ~/.zshenv
#

[[ -f ~/.profile ]] && . ~/.profile

function antibodyRegen() {
  antibody bundle < "$HOME/.config/antibody" > "$HOME/.cache/antibodyrc"
}
