#
# ~/.zshrc
#

[[ -f ~/.globalrc ]] && . ~/.globalrc

bindkey -e

autoload -Uz compinit
autoload -U +X bashcompinit

setopt interactivecomments

setopt inc_append_history
setopt share_history

# Load Powerlevel10K Config
[[ -f ~/.p10k.zsh ]] && source ~/.p10k.zsh

# zsh completion generator

GENCOMPL_FPATH=$HOME/.zsh/complete

zstyle ':completion:*' menu select setopt COMPLETE_ALIASES
zstyle :plugin:zsh-completion-generator programs ranger

# brew (macOS)
# Note: Before compinit
if ( whence brew &> /dev/null ); then
  fpath+="$(brew --prefix)$/share/zsh/site-functions"
fi

# iTerm2 (macOS)
# Note: Full terminal integration
if [[ -f ~/.iterm2_shell_integration.zsh ]]; then
  source "${HOME}/.iterm2_shell_integration.zsh"
fi

# Recommended by poetry docs
# Needs more investigation
# fpath+=~/.zfunc

# caches compinit dump for 24 hrs
# to avoid rebuild times
if [[ -n ${ZDOTDIR:-${HOME}}/$ZSH_COMPDUMP(#qN.mh+24) ]]; then
  compinit -d $ZSH_COMPDUMP;
else
  compinit -C;
fi;

bashcompinit

# pipx
if [[ -f ~/.local/bin/pipx ]]; then
  eval "$(register-python-argcomplete pipx)"
fi

# mc for Minio
# if [[ -f /usr/local/bin/mc ]]; then
#   complete -o nospace -C /usr/local/bin/mc mc
# fi

# antigen
if [[ -f ~/.config/antigen ]]; then
  # Manual install location
  if [[ -f ~/.cache/antigen.zsh ]]; then
    source "$HOME/.cache/antigen.zsh"
  # macOS (brew) install location
  elif [[ -f /opt/homebrew/share/antigen/antigen.zsh ]]; then
    source /opt/homebrew/share/antigen/antigen.zsh
  # Ubuntu (zsh-antigen) install location
  elif [[ -f /usr/share/zsh-antigen/antigen.zsh ]]; then
    source /usr/share/zsh-antigen/antigen.zsh
  fi

  if ( whence antigen &> /dev/null ); then
    antigen init ~/.config/antigen
  fi
fi

# antibody
if [[ -f ~/.config/antibody ]]; then
  if ( whence antibody &> /dev/null ); then
    # Generate antibody cache if necessary
    if [[ ! -f ~/.cache/antibodyrc ]]; then
      antibody bundle < "$HOME/.config/antibody" > "$HOME/.cache/antibodyrc"
    fi
  fi

  # Load antibody cache if exists
  if [[ -f ~/.cache/antibodyrc ]]; then
    source "$HOME/.cache/antibodyrc"
  fi
fi

# syntax-highlighting
ZSH_HIGHLIGHT_STYLES[comment]='fg=10'

# autosuggestions
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=10"
