const regedit = require('regedit');

const template = {
  "name": "",
  "author": "",
  "color": [
    "#000000",
    "#dc322f",
    "#859900",
    "#b58900",
    "#268bd2",
    "#d169d1",
    "#2aa198",
    "#b0b0b0",
    "#5d5d5d",
    "#e09690",
    "#cdee69",
    "#ffe377",
    "#9cd9f0",
    "#fbb1f9",
    "#77dfd8",
    "#f7f7f7"
  ],
  "foreground": "#93a1a1",
  "background": "#0e2a2e"
};

let colors = Array(16).fill('#000000');

// Values stored as 00-BB-GG-RR
// BLACK DGRAY
colors[0] = template['background'];
colors[8] = template['color'][8];
// BLUE LBLUE
colors[1] = template['color'][4];
colors[9] = template['color'][12];
// GREEN LGREEN
colors[2]=template['color'][2]
colors[10]=template['color'][10]
// CYAN LCYAN
//colors[3]=template['color'][3]
//colors[11]='00d3d08c'
// RED LRED
//colors[4]='00232333'
//colors[12]='007071e3'
// MAGENTA LMAGENTA
//colors[5]='00aa50aa'
//colors[13]='00c880c8'
// YELLOW LYELLOW
//colors[6]='0000dcdc'
//colors[14]='00afdff0'
// LGRAY WHITE
//colors[7]='00ccdcdc'
//colors[15]='00ffffff'


regedit.putValue({
    'HKCU\\Console\\C:_Program Files_WindowsApps_CanonicalGroupLimited.UbuntuonWindows_1804.2019.521.0_x64__79rhkp1fndgsc_ubuntu.exe': colors.reduce((obj, val, idx) => {
        obj[`ColorTable${idx.padStart(2, '0')}`] = {
            value: '00' + val.substr(1).match(/.{1,2}/g).reverse().join(''),
            type: 'REG_DWORD'
        };
        return obj;
    }, {})
}, console.error);
