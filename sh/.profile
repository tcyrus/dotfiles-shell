# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

export QT_QPA_PLATFORMTHEME="qt5ct"

if [[ -f /usr/local/bin/nano ]]; then
    EDITOR=/usr/local/bin/nano
else
    EDITOR=/usr/bin/nano
fi

export EDITOR

export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
export BROWSER=/usr/bin/chromium

# better yaourt colors
export YAOURT_COLORS="nb=1:pkg=1:ver=1;32:lver=1;45:installed=1;42:grp=1;34:od=1;41;5:votes=1;44:dsc=0:other=1;35"

[[ -f ~/.aliases ]] && . ~/.aliases

if [ -n "$DESKTOP_SESSION" ]; then
    eval $(gnome-keyring-daemon --start)
    export SSH_AUTH_SOCK
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.bin" ]; then
    PATH="$HOME/.bin:$PATH"
fi
if [ -d "$HOME/bin" ]; then
    PATH="$HOME/bin:$PATH"
fi
if [ -d "$HOME/.local/bin" ]; then
    PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/.go" ]; then
    export GOPATH="$HOME/.go"
    PATH="$GOPATH/bin:$PATH"

    GOPREFIX="/usr/local/opt/go"
    if [ -d "$GOPREFIX" ]; then
        export GOROOT="$GOPREFIX/libexec"
        PATH="$GOROOT/bin:$PATH"
    fi
fi

[[ -f ~/.cargo/env ]] && . ~/.cargo/env

if [ -d "$HOME/.cargo/bin" ]; then
    PATH="$HOME/.cargo/bin:$PATH"
fi

if [ -d "$HOME/.yarn/bin" ]; then
    PATH="$HOME/.yarn/bin:$PATH"
fi

if [ -d "$HOME/.poetry/bin" ]; then
    PATH="$HOME/.poetry/bin:$PATH"
fi

if [ -d "$HOME/.gem/ruby/2.7.0" ]; then
    export GEM_HOME="$HOME/.gem/ruby/2.7.0"
    PATH="$GEM_HOME/bin:$PATH"
elif [ -d "$HOME/.gem/ruby/2.6.0" ]; then
    export GEM_HOME="$HOME/.gem/ruby/2.6.0"
    PATH="$GEM_HOME/bin:$PATH"
elif [ -d "$HOME/.gem/ruby/2.5.0" ]; then
    export GEM_HOME="$HOME/.gem/ruby/2.5.0"
    PATH="$HOME/.gem/ruby/2.5.0/bin:$PATH"
fi
#if [ -d "/usr/local/lib/ruby/gems/2.6.0/bin" ]; then
#    PATH="/usr/local/lib/ruby/gems/2.6.0/bin:$PATH"
#fi
if [ -d "/usr/local/opt/ruby/bin" ]; then
    PATH="/usr/local/opt/ruby/bin:$PATH"
fi

if [ -d "/var/ipfs" ]; then
    export IPFS_PATH=/var/ipfs
fi

if [ -d "$HOME/.profile.d" ]; then
    for file in "$HOME/.profile.d"/.*; do
        . "$file"
    done
fi

export PATH

